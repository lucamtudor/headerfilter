
package com.example.headerfilter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.TextView;

public class HeaderFilterView extends FrameLayout implements View.OnClickListener, AnimationListener {

    public static final int ANIMATION_DURATION = 300;

    private boolean mIsExpanded = false;
    private boolean mIsAnimating = false;
    private int mItemHeight;
    private int mInitialHeight;
    private int mTotalHeight;
    private int mPadding;

    private int mSelectedIndex = 0;
    private OnFilterSelectedListener mListener;

    public enum Filter {
        Global, Home, Brands, Friends
    }

    public interface OnFilterSelectedListener {

        public void onFilterSelected(int filterIndex);

    }

    public void setOnFilterSelectedListener(OnFilterSelectedListener listener) {
        this.mListener = listener;
    }

    public HeaderFilterView(Context context) {
        super(context);
        init();
    }

    public HeaderFilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @SuppressLint("NewApi")
    public HeaderFilterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mPadding = toPixels(10);
        setPadding(mPadding, mPadding, mPadding, 0);

        TextView mFilterTextView = generateFilterTextView();
        int mMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        mFilterTextView.measure(mMeasureSpec, mMeasureSpec);
        mFilterTextView.setSelected(true);
        mFilterTextView.setText(Filter.values()[0].name());

        mItemHeight = mFilterTextView.getMeasuredHeight();
        mInitialHeight = mItemHeight + mPadding;
        mTotalHeight = mItemHeight * 4 + mPadding;
        addView(mFilterTextView);

        for (int i = 1; i < Filter.values().length; i++) {
            TextView filter = generateFilterTextView();
            filter.setText(Filter.values()[i].name());
            addView(filter);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int childTop = 0 + mPadding;
        int childLeft = 0 + mPadding;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.layout(childLeft, childTop, r, childTop + mItemHeight);
            childTop += mItemHeight;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int mMeasuredWidth = getMeasuredWidth();
        int mMeasuredHeight = getMeasuredHeight();

        if (!mIsExpanded && !mIsAnimating) {
            setMeasuredDimension(mMeasuredWidth, mInitialHeight);
        } else {
            setMeasuredDimension(mMeasuredWidth, mMeasuredHeight);
        }
    }

    private void toggle() {
        if (mIsExpanded) {
            collapse();
        } else {
            expand();
        }
    }

    private void expand() {
        if (mIsAnimating) {
            return;
        }
        AnimationSet mAnimationSet = new AnimationSet(true);
        mAnimationSet.addAnimation(new ExpandAnimation());
        mAnimationSet.addAnimation(new ScrollToAnimation(0));
        mAnimationSet.setAnimationListener(this);
        startAnimation(mAnimationSet);
    }

    private void collapse() {
        if (mIsAnimating) {
            return;
        }
        AnimationSet mAnimationSet = new AnimationSet(true);
        mAnimationSet.addAnimation(new ExpandAnimation());
        mAnimationSet.addAnimation(new ScrollToAnimation());
        mAnimationSet.setAnimationListener(this);
        startAnimation(mAnimationSet);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mIsAnimating = false;
        mIsExpanded = !mIsExpanded;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onAnimationStart(Animation animation) {
        mIsAnimating = true;
    }

    @Override
    public void onClick(View v) {
        if (mIsAnimating) {
            return;
        }
        if (mIsExpanded) {
            getChildAt(mSelectedIndex).setSelected(false);
            mSelectedIndex = indexOfChild(v);
            v.setSelected(true);
            if (mListener != null) {
                mListener.onFilterSelected(mSelectedIndex);
            }
        }
        toggle();
    }

    private TextView generateFilterTextView() {
        TextView filterTextView = new TextView(getContext());
        filterTextView.setBackgroundResource(R.drawable.filter_selector);
        int mPadding = toPixels(10);
        filterTextView.setPadding(mPadding, mPadding, mPadding, mPadding);
        filterTextView.setGravity(Gravity.CENTER_VERTICAL);
        filterTextView.setOnClickListener(this);
        filterTextView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        return filterTextView;
    }

    private int toPixels(int dp) {
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    class ScrollToAnimation extends Animation {

        private View mAnimatedView;
        private int mScrollStart, mScrollEnd;
        private boolean mWasEndedAlready = false;

        public ScrollToAnimation() {
            this(HeaderFilterView.this.mItemHeight * HeaderFilterView.this.mSelectedIndex);
        }

        public ScrollToAnimation(int scrollEnd) {
            setDuration(HeaderFilterView.ANIMATION_DURATION);
            mAnimatedView = HeaderFilterView.this;
            mScrollStart = mAnimatedView.getScrollY();
            mScrollEnd = scrollEnd;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);

            if (interpolatedTime < 1.0f) {
                int mCurrentScroll = mScrollStart + (int)((mScrollEnd - mScrollStart) * interpolatedTime);
                mAnimatedView.scrollTo(0, mCurrentScroll);
            } else if (!mWasEndedAlready) {
                mAnimatedView.scrollTo(0, mScrollEnd);
                mWasEndedAlready = true;
            }
        }
    }

    class ExpandAnimation extends Animation {

        private View mAnimatedView;
        private ViewGroup.LayoutParams mViewLayoutParams;
        private int mMarginStart, mMarginEnd;
        private boolean mWasEndedAlready = false;

        public ExpandAnimation() {
            setDuration(HeaderFilterView.ANIMATION_DURATION);
            mAnimatedView = HeaderFilterView.this;
            mViewLayoutParams = (ViewGroup.LayoutParams)mAnimatedView.getLayoutParams();
            mMarginStart = HeaderFilterView.this.mIsExpanded ? HeaderFilterView.this.mTotalHeight : HeaderFilterView.this.mInitialHeight;
            mMarginEnd = !HeaderFilterView.this.mIsExpanded ? HeaderFilterView.this.mTotalHeight : HeaderFilterView.this.mInitialHeight;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);

            if (interpolatedTime < 1.0f) {
                mViewLayoutParams.height = mMarginStart + (int)((mMarginEnd - mMarginStart) * interpolatedTime);
                mAnimatedView.requestLayout();
            } else if (!mWasEndedAlready) {
                mViewLayoutParams.height = mMarginEnd;
                mAnimatedView.requestLayout();
                mWasEndedAlready = true;
            }
        }
    }

}
