
package com.example.headerfilter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView mListView = (ListView)findViewById(R.id.list);
        String[] items = new String[] {
                "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item",
                "item", "item", "item", "item"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        mListView.addHeaderView(new HeaderFilterView(this));
        mListView.setAdapter(adapter);
    }
}
